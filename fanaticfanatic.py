from bs4 import BeautifulSoup
import urllib.request
import json
import smtplib, ssl
from python_mailer import SendMail
import os
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email.encoders import encode_base64
import datetime

###################################################################################
# Configuration

SmtpServer = "smtp.mail.yahoo.com"
SmtpPort = 587
SmtpUser = 'trukkosdezso'
# note: http://napirajz.hu/?p=1053
SmtpPassword = 'uybcjsekegbcyiwy'
FromAddress = 'trukkosdezso@yahoo.com'
ToAddress = 'fanaticfanatic@mailinator.com'
Subject = 'fanaticfanatic report'
knownAdsFile = 'known_ads.json'

# pages to scrape:
PagesToScrape = ['https://gsfanatic.com/hu/hangszerek/dobok-6--utosok/pergodob',
            'https://gsfanatic.com/hu/hangszerek/dobok-6--utosok/all']

####################################################################################
def soupify(url):
    with urllib.request.urlopen(url) as response:
        html = response.read().decode('utf8')
    return BeautifulSoup(html, 'html.parser')

def getSubPages(page):
    # gs fanatic anomaly: 1st subpage contains 3x9 ads, higher subpages contain 3x8 ads
    # number of subpages should be taken from page 2
    soup = soupify(page)
    l = soup.find_all('li')
    for li in l:
        try:
            if 'page=2' in li.a['href']:  # extract subpage base link
                baseLink = '='.join(li.a['href'].split('=')[:-1:]) + '='
                page2soup = soupify(baseLink+'2')
                l2 = page2soup.find_all('li')  # go to page 2
                for li2 in l2:  # extract number of pages
                    try:
                        if '...' in li2.text:
                            numOfPages = int(li2.a['href'].split('=')[::-1][0])
                            subPages = [baseLink + str(page) for page in range(1, numOfPages+1)]
                            return subPages
                    except:
                        return []
        except KeyError:
            pass
    return [page]

# scrape a page and extract individual ads/sub-pages
def extractAds(page):
    soup = soupify(page)
    d = soup.find_all('div')
    adlinks = []
    for div in d:
        try:
            if 'hirdeteslist_hirdetes' in div['class']:
                href = div.a['href']
                adlinks.append(href.replace('\'', '').replace('\\',''))
        except KeyError:
            pass
    return adlinks

# scrape sub-page for summary
def scrapeAd(url):
    adsoup = soupify(url)
    return adsoup.title.string

def sendMail(e, echo=True):
    """ sendMail method. Inputs: e: dictonary, returns: (error)code, (error)info"""
    assert type(e["files"])==list # check if files have been given then they are in a list
    code, info = 999, "default"
    msg = MIMEMultipart()
    msg['From'] = e["From"]
    msg['To'] = e["To"] #COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = e["Sub"]
    msg.attach( MIMEText(e["Text"]) )
    for file in e["files"]:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(file,"rb").read() )
        encode_base64(part)             # Python 3
        part.add_header('Content-Disposition', 'attachment; filename="%s"'% os.path.basename(file))
        msg.attach(part)
    if e["SSL"]=="Yes":
        s = smtplib.SMTP(e["Server"], e["Port"])
        #s = smtplib.SMTP(SmtpServer, SmtpPort)
        s.ehlo()
        s.starttls()
        if echo:
            s.set_debuglevel(1)
        else:
            s.set_debuglevel(0)
        s.ehlo()
        s.login(e["Authlogin"], e["Authpass"])
        s.sendmail(msg["From"], msg["To"], msg.as_string() )
        s.close()
    else: # SSL=="No"
        s = smtplib.SMTP(e["Server"])
        s.set_debuglevel(1)
        s.ehlo()
        s.sendmail(msg["From"], msg["To"], msg.as_string() )
        s.close()
    return code, info

if __name__ == '__main__':
    lastReport = None
    knownURLs = []
    # open file with entries from last run -> knownURLs: containing all the known ads
    try:
        with open(knownAdsFile, "r") as file:
            knownURLs = json.load(file)
    except (FileNotFoundError, json.JSONDecodeError):
        pass  # leave the knowURLs list empty
    
    # parse subpages:
    subPages = []
    for page in PagesToScrape:
        subPages += getSubPages(page)
    
    # extract ads from all subpages -> ads: containing all the urls
    ads = []
    for k, subpage in enumerate(subPages):
        # print('subpage: '+str(k+1)+'/'+str(len(subPages)))
        ads += extractAds(subpage)
    # clean-up ads: list with unique values only
    print(datetime.datetime.now())
    print('Scraping sources: ')
    [print(x) for x in PagesToScrape]
    print('Scraping '+str(len(ads))+' ads on '+str(len(subPages))+' subpages.' )
    ads = set(ads)

    # get new ads that are not among the known URLs
    newAds = [url for url in ads if url not in knownURLs]
    
    # clean-up knownURLs: remove urls that are not among the current ads (deleted obsolete ads)
    knownURLs = [url for url in knownURLs if url in ads] + newAds
    
    report = []
    for k, adpage in enumerate(newAds):
        newEntry = {}
        newEntry['url'] = adpage
        summary = scrapeAd(adpage)
        newEntry['summary'] = summary
        print('new ad: '+str(k+1) +'/'+ str(len(newAds))+':'+summary)
        report.append(newEntry)
    
    # save the known ads for later use 
    with open(knownAdsFile, "w+") as file:
        json.dump(knownURLs, file, separators=(',\n', ':'))
            
    if len(report)>0:
        mail = {}
        mail['From'] = FromAddress
        mail['To'] = ToAddress
        mail['Sub'] = Subject
        mail['Text'] = ''
        mail['files'] = []
        mail['SSL'] = 'Yes'
        mail['Server'] = SmtpServer
        mail['Port'] = SmtpPort
        mail['Authlogin'] = SmtpUser
        mail['Authpass'] = SmtpPassword
        for item in report:
            mail['Text'] += str(item['summary']) + '\n' + str(item['url']) + '\n\n'
        try:
            code, info = sendMail(mail, echo=False)
        except Exception as e:
            print('Error while sending mail.', e)
        else:
            print('New ads sent to: '+ToAddress)
    else:
        print('No new ads found.')