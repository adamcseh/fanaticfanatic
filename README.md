Webscraping script for getting the latest ads from gsfanatic.
The script goes through the pages of interest, extract the ads, compares them to a list of known ads and sends an email containing the new ones.

Use the 'Configuration' field in fanaticfanatic.py to configure various settings and pages of interest.

The fanaticfanatic.sh can be used to handle the virtual environment while running as a shell script
Note: the VIRTUAL_ENV variable in /env/bin/activate might need to be changed to reflect the actual path.
