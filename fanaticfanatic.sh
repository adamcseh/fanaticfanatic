#!/bin/sh
if test -f env/bin/activate; then
	echo 'Trying to run in virtualenv...'
	source env/bin/activate
	python3 fanaticfanatic.py
	deactivate
else
	python3 fanaticfanatic.py
fi

