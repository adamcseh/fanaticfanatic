
# -*- coding: utf-8 -*-
####################
"""python-mailer.py : For sending emails thru SMTP server with authentication"""
# Main script : python_mailer.py
# Author: Peter Vago, 2010
####################

class SendMail():
    """SendMail class: class for sending email"""
    def sendMail(self,e):
        """ sendMail method. Inputs: e: dictonary, returns: (error)code, (error)info"""
        import os
        #import smtplib
        import config

        # # Python 2
        # from email.MIMEMultipart import MIMEMultipart
        # from email.MIMEBase import MIMEBase
        # from email.MIMEText import MIMEText
        # from email.Utils import COMMASPACE, formatdate
        # from email import Encoders

        # Python 3
        from email.mime.multipart import MIMEMultipart
        from email.mime.base import MIMEBase
        from email.mime.text import MIMEText
        from email.utils import COMMASPACE, formatdate
        from email.encoders import encode_base64

    #    assert type(to)==list
        assert type(e["files"])==list # check if files have been given then they are in a list

        code, info = 999, "default"

        msg = MIMEMultipart()
        msg['From'] = e["From"]
        msg['To'] = e["To"] #COMMASPACE.join(to)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = e["Sub"]

        msg.attach( MIMEText(e["Text"]) )
    #    print msg

        for file in e["files"]:
    #	print file
            part = MIMEBase('application', "octet-stream")
            part.set_payload( open(file,"rb").read() )
            #Encoders.encode_base64(part)   # Python 2
            encode_base64(part)             # Python 3
            part.add_header('Content-Disposition', 'attachment; filename="%s"'
                           % os.path.basename(file))
            msg.attach(part)

        if e["SSL"]=="Yes":
            s = smtplib.SMTP_SSL()
            s.connect(e["Server"], 465)
        #    	s = smtplib.SMTP(e["Server"])
            s.set_debuglevel(1)
            s.ehlo()
        #    	s.starttls()
        #    	s.ehlo()
            s.login(e["Authlogin"], e["Authpass"])
            s.sendmail(msg["From"], msg["To"], msg.as_string() )
            s.close()
        else: # SSL=="No"
        #	s = smtplib.SMTP_SSL()
        #	s.connect(e["Server"], 465)
            s = smtplib.SMTP(e["Server"])
            s.set_debuglevel(1)
            s.ehlo()
            #s.starttls()
            #s.login(e["Authlogin"], e["Authpass"])
            s.sendmail(msg["From"], msg["To"], msg.as_string() )
        ##	s.sendmail(msg["From"], msg["To"], msg )
            s.close()


        return code, info
#=======================

class mylogging():
    mylogger = "" # declare only
    import logging
    def __init__(self, logfile="/tmp/mylogger.log", LOGGINGLVL=logging.DEBUG):
        import logging
        # Initialize logger
        logformat = "%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s"
        datefmt = "%m-%d %H:%M"
        logging.basicConfig(filename=logfile, level=LOGGINGLVL, filemode="a", format=logformat, datefmt=datefmt)
        stream_handler = logging.StreamHandler(sys.stderr)
        stream_handler.setFormatter(logging.Formatter(fmt=logformat, datefmt=datefmt))
        self.mylogger = logging.getLogger("app")
        self.mylogger.addHandler(stream_handler)

    def getlogger_reference(self):
        # Data accessor
        return self.mylogger

    def change_debuglevel(self, newlevel):
        # critical, error, warning, info, debug, notset" # https://docs.python.org/2/library/logging.html
        lvl_list = ["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"]
        nl = newlevel.upper()
        if any(nl in l for l in
               lvl_list):  # True if nl is matching to any string or ANY SUBSTRING in the list. E.g. "NING" -> WARNING :D
            nl_suppl = [s for s in lvl_list if nl in s]  # Type: list!, Supplements , eg. "WARN" -> "WARNING"
            logging.getLogger().setLevel(nl_suppl[0])
            self.mylogger.debug("newlevel:%s" % nl_suppl)
            return 0
        else:
            print("Debug level is invalid: '%s' (expected: %s) /non case-sensitive/" % (newlevel, str(lvl_list)))
            return 23
def main(args):
    """ Main function """
    # Enable logging
    import logging
    global logger # declare variable as global
    l = mylogging("/tmp/mylogger.log", logging.DEBUG)
    logger = l.getlogger_reference() # Create object and then Get object referenc

    code, info = 998,"default"

    logger.debug("Arguments: "+str(args))
    if len(args) < 4:
        To = "peter.vago@gmail.com"
        Sub = "Mailer-teszt"
        Text = "Keves argumentum \n\n"
        logger.error("/etc/nagios3/python-mailer/python-mailer.py <Destaddress> <Subject> <Body>\n")
        return 2, "/etc/nagios3/python-mailer/python-mailer.py <Destaddress> <Subject> <Body>\n"
    else:
        To = args[1]
        Sub = args[2]
        Text = args[3]
        Text.replace("\\n", "\n")

    import config
    e = {"From": config.email_address, "Sub": Sub, "Text": Text, "To": To, "files": [], "Server": config.smtp_server,"Authlogin": config.username, "Authpass": config.password, "SSL": config.ssl}
    logger.debug(e)
    s = SendMail()
    code, info = s.sendMail(e)

    return code, info


if __name__ == "__main__": # if you call this py file directly (== not as a module), then this call the main function 
    import sys
    """ Usage: python_mailer.py "Destaddress" "Subject" "Body" """
    arguments=sys.argv

    # Here we can pass any test arguments ;)
    additional_args = []
    #Test cases
    from datetime import datetime
    now=datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    #additional_args = ["peter.vago@gmail.com","Test_message-"+str(now),"Test body. "+str(now)]
    #additional_args = ["peter.vago@noffz.com","Test_message-"+str(now),"Test body. "+str(now)]

    for ar_g in additional_args:
        arguments.append(ar_g)

    retcode, info =main(arguments)
    sys.exit(retcode)

